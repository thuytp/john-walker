module.exports = function(grunt) {
	// Displays the elapsed execution time of grunt tasks
	require('time-grunt')(grunt);

	// Load NPM Task
	require('load-grunt-tasks')(grunt, ['grunt-*']);

	grunt.initConfig({
		// Define source and build folders
		build: 			'dist',
		css_build: 		'<%= build %>/assets/css',
		js_build: 		'<%= build %>/assets/js',
		img_build: 		'<%= build %>/assets/images',
		vendor: 		'<%= build %>/assets/vendors',

		src: 			'source',
		css_src: 		'<%= src %>/assets/sass',
		js_src: 		'<%= src %>/assets/js',
		img_src: 		'<%= src %>/assets/images',

		// Ejs
		ejs: {
			all: {
				src: ['**/*.ejs', '!_partials/**/*'],
				dest: '<%= build %>/',
				cwd: '<%= src %>',
				expand: true,
				ext: '.html'
			}
		},

		// Minify html
		htmlmin: {
			dist: {
				options: {
					removeComments: true,
					collapseWhitespace: true,
					removeEmptyAttributes: true,
					removeCommentsFromCDATA: true,
					removeRedundantAttributes: true,
					collapseBooleanAttributes: true
				},
				files: {
					'<%= build %>/index.html': '<%= build %>/index.html'
				}
			}
		},

		// Concat js
		concat: {
			options: {
				separator: ';'
			},
			dist: {
				src: ['<%= src %>/assets/js/**/*.js'],
				dest: '<%= build %>/assets/js/built.js'
			}
		},

		jshint: {
			beforeconcat: ['Gruntfile.js', '<%= src %>/assets/js/**/*.js'],
			afterconcat: ['<%= build %>/assets/js/**/*.js']
		},

		// Sass
		sass: {
			dist: {
				options: {
					style: 'expanded', // nested, compact, compressed, expanded,
					sourcemap: true
				},
				files: {
					'<%= build %>/assets/css/main.min.css': '<%= src %>/assets/sass/main.scss'
				}
			},
			dev: {
				options: {
					style: 'expanded'
				},
				files: {
					src: ['<%= src %>/assets/sass/**/*.scss'],
					dest: '<%= build %>/assets/css/**/*.css'
				}
			}
		},

		autoprefixer: {
			dist: {
				options: {
					map: true
				},
				files: {
					'<%= build %>/assets/css/main.min.css': '<%= build %>/assets/css/main.min.css'
				}
			}
		},

		// Minify images
		imagemin: {
			img: {
				options: {
					progressive: true,
					interlaced: true,
				},
				files: [{
					expand: true,
					cwd: '<%= img_src %>/',
				  src: [
					'**/*.{png,jpg,jpeg,gif,svg}'
				  ],
				  dest: '<%= img_build %>'
				}],
			}
		},

		// Watch
		watch: {
			options: {
				interval: 200
			},

			sass: {
				files: ['<%= src %>/assets/sass/**/*.scss'],
				tasks: ['sass', 'autoprefixer']
			},

			css: {
				files: ['<%= build %>/assets/css/**/*.css'],
				task: ['autoprefixer'],
				livereload: true
			},

			ejs: {
				files: ['<%= src %>/**/*.ejs'],
				tasks: ['ejs']
			},

			html: {
				files: ['<%= build %>/*.html'],
				tasks: [],
				livereload: true
			},

			js: {
				files: ['<%= src %>/assets/js/**/*.js', '!<%= src %>/assets/js/**/*.min.js'],
				tasks: ['jshint', 'concat'],
				livereload: true,
				options: {
					spawn: false
				}
			}
		},

		// Copy
		copy: {
			main: {
				files: [
					{
						expand: true,
						src: ['**/*'],
						dest: '<%= build %>/assets/fonts',
						cwd: '<%= src %>/assets/fonts'
					}
				]
			}
		},

		// Browser Sync
		browserSync: {
			bsFiles: {
				src: [
					'<%= build %>/assets/css/**/*.css',
					'<%= build %>/assets/js/**/*.js',
					'<%= build %>/assets/**/*.{jpg, png, svg, gif}',
					'<%= build %>/assets/fonts/**/*',
					'<%= build %>/*.html'
				]
			},
			options: {
				watchTask: true,
				host: '',
				server: {
					baseDir: '<%= build %>/'
				},
				ghostMode: {
					clicks: false,
					scroll: false,
					links: false,
					forms: false
				}
			}
		}
	});

	// Grunt register
	grunt.registerTask('default', ['ejs', 'copy', 'sass', 'autoprefixer', 'concat', 'imagemin', 'jshint', 'browserSync', 'watch']);
};
